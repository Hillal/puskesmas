<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionnaireTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaire', function (Blueprint $table) {
            $table->uuid('id')->primary()->default(\Ramsey\Uuid\Uuid::uuid4());
            $table->uuid("user_id")->nullable();
            $table->string('persyaratan');
            $table->string('prosedur');
            $table->string('kecepatan');
            $table->string('biaya');
            $table->string('produk_pelayanan');
            $table->string('kompetensi');
            $table->string('perilaku');
            $table->string('sarana_prasarana');
            $table->string('pengaduan_layanan');
            $table->string('saran', 500);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionnaire');
    }
}

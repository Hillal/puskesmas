<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    public $incrementing = false;

    protected $table = 'questionnaire';

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}

<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Questionnaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;
use Ramsey\Uuid\Uuid;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'persyaratan' => 'required',
            'prosedur' => 'required',
            'kecepatan' => 'required',
            'biaya' => 'required',
            'produk_pelayanan' => 'required',
            'kompetensi' => 'required',
            'perilaku' => 'required',
            'sarana_prasarana' => 'required',
            'pengaduan_layanan' => 'required',
            'saran' => 'required',
        ]);

        try{
            $userId =Session::get('userId', "");

            $Questionnaire = new Questionnaire();
            $Questionnaire->id = Uuid::uuid4();
            $Questionnaire->user_id = $userId;
            $Questionnaire->persyaratan = $request->persyaratan;
            $Questionnaire->prosedur = $request->prosedur;
            $Questionnaire->kecepatan = $request->kecepatan;
            $Questionnaire->biaya = $request->biaya;
            $Questionnaire->produk_pelayanan = $request->produk_pelayanan;
            $Questionnaire->kompetensi = $request->kompetensi;
            $Questionnaire->perilaku = $request->perilaku;
            $Questionnaire->sarana_prasarana = $request->sarana_prasarana;
            $Questionnaire->pengaduan_layanan = $request->pengaduan_layanan;
            $Questionnaire->saran = $request->saran;

            $Questionnaire->save();

            $code = 200;
            $response = $Questionnaire;
        }catch (\Exception $e){
            if ($e instanceof ValidationException){
                $code = 400;
                $response = 'tidak ada data';
            }else{
                $code = 500;
                $response = $e->getMessage();
            }
        }

        return apiResponseBuilder($code,$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

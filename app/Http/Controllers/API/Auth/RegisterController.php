<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\User;
use Exception;
use Ramsey\Uuid\Uuid;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $user = new User;

        $validator = Validator::make(request()->all(), [
            'name' => 'required|max:100',
            'password' => 'required|min:6|max:100',
            'email' => 'required|max:100|email',
            'confirmPass' => 'required|in:' . $request->input('password'),
        ]);

        if ($validator->fails()){
            return apiResponseValidationFails('Validation Error message!', $validator->errors()->all());
        } else {
            $userEmail = User::where('email', $request->email)->first();
            if ($userEmail != null){
                return apiResponseValidationFails('Email Sudah Terdaftar'. $validator->errors()->all());
            }

            $user->id = Uuid::uuid4();
            $user->name = $request->name;
            $user->password = Hash::make($request->password);
            $user->email = $request->email;
            $user->role = 'customer';

            $user->save();

            $success['user'] = $user;
            $success['token'] = $user->createToken('myApp')->accessToken;

            Session::put('userId', $user->id);
            Session::put('userRole', 'customer');

            return apiResponseSuccess('Register Success!', $success, 200);
        }

        return apiResponseBuilder($code, $response);
    }
}

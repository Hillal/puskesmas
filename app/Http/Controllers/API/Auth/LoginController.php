<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function login(Request $request){
        $email = $request->input('email');
        $password = $request->input('password');

        $validator = Validator::make(\request()->all(), [
            'email' => 'required|max:100|email',
            'password' => 'required|min:6|max:100',
        ]);

        if ($validator->fails()){
            return apiResponseValidationFails('Validation Error Message!', $validator->errors()->all());
        } else {
            $user = User::where('email', $email)->first();

            if ($user == null){
                return apiResponseValidationFails('Email Tidak Terdaftar', $validator->errors()->all());
            }

            if (!Hash::check($password, $user->password)){
                return apiResponseValidationFails('password tidak sesuai', $validator->errors()->all());
            }

            $success['user'] = $user;
            $success['token'] = $user->createToken('myApp')->accessToken;

            Session::put('userId', $user->id);
            Session::put('userRole', 'customer');

            return apiResponseSuccess('Login Success!', $success, 200);
        }

        return apiResponseBuilder($code, $response);
    }
}
